"""
conversion
"""

import sys
import os
import numpy as np
import argparse
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from natsort import natsorted

import time

#import matplotlib as mpl
#mpl.use('Qt5Agg') #comment if not installed, 

from matplotlib import cm

from pygenestats.common.logger import logger


def process(in_csv_fpath,
                config_fpath,
                output_fpath,
                in_type="geneatic_exportgramps"):
    """


    Args:
        in_type = "geneatic_exportgramps"
        in_type = "gramps"

    Raises:

    Returns:
    """

    people_d = read_people(in_csv_fpath=in_csv_fpath, in_type=in_type)
    marriages_d = read_marriages(in_csv_fpath=in_csv_fpath)
    child_d = read_family_child(in_csv_fpath=in_csv_fpath)
    logger.info("People_dico: {}".format(people_d))

    convert_config_d = load_conversion_config(config_fpath=config_fpath)
    #find mmpp pids
    mmpp_codes = ["ppa", "mma", "ppi", "mmi"]
    mmpp_pids = {}
    data_permmppcode = {}
    for mmppcode in mmpp_codes:
        _pid = find_mmpp_pid(mmpp_tag=mmppcode, 
                            Name=convert_config_d[mmppcode]["gedcom_nom"], 
                            Given=convert_config_d[mmppcode]["gedcom_prenom"], 
                            birth_year=convert_config_d[mmppcode]["gedcom_annee_naissance"], 
                            people_d=people_d)
        mmpp_pids[mmppcode] = _pid
        if _pid is not None:
            _people = people_d[_pid]
            data_permmppcode[mmppcode] = people_d[_pid]
            data_permmppcode[mmppcode]["pid"] = _pid
        else:
            _people = None
        logger.info("Found pid={}, \n{}".format(_pid, _people))
        input("Press any key")
   

    codes_l = get_all_codes_pergene(data_permmppcode, 0)
    logger.info("codes_l: {}".format(codes_l))

    # for each generation, find parents
    n_gen_load = 30
    for kgene in range(0, n_gen_load):
        logger.info("processing mmp for kgene={}".format(kgene))
        codes_l = get_all_codes_pergene(data_permmppcode, kgene)
        logger.info("codes_l: {}".format(codes_l))
        for _code in codes_l:
            _child_pid = data_permmppcode[_code]["pid"]
            _pids = get_parents_pids(pid=_child_pid, child_d=child_d, marriages_d=marriages_d)
            for _pid in _pids:
                if _pid in people_d.keys():
                    if people_d[_pid]["sexe"] == "male":
                        if get_code_gen(_code)==0:
                            _new_code = "{}_p".format(_code)
                        else:
                            _new_code = "{}p".format(_code)
                            
                    else:
                        if get_code_gen(_code)==0:
                            _new_code = "{}_m".format(_code)
                        else:
                            _new_code = "{}m".format(_code)
                        
                    data_permmppcode[_new_code] = people_d[_pid]
                    data_permmppcode[_new_code]["pid"] = _pid
                    

    logger.info("data_permmppcode : {}".format(data_permmppcode))
    logger.info("data_permmppcode, N={}".format(len(data_permmppcode)))

    write_mmppcode_file(out_fpath=output_fpath, data_permmppcode=data_permmppcode)


def write_mmppcode_file(out_fpath, data_permmppcode):
    """
    """

    with open(out_fpath, "w") as fout:
        fout.write("#Nom\tPrenom\tCode\tMétier\tNaissance\tNaissance_annee\tNaissance_ville\tNaissance_pays\tMariage\tDeces\n")
        for key in data_permmppcode:
            prenom_raw = data_permmppcode[key]["given"]
            prenom_list = prenom_raw.split(" ")
            prenom_formatted = ",".join(prenom_list)
            fout.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(
                data_permmppcode[key]["name"],
                prenom_formatted,
                key,
                "-",
                data_permmppcode[key]["birth_date"],
                data_permmppcode[key]["birth_year"],
                data_permmppcode[key]["city"],
                data_permmppcode[key]["country"],
                "-",
                data_permmppcode[key]["death_year"]))



def get_parents_pids(pid, child_d, marriages_d):
    """
    """

    #find family for this child
    family_key = None
    for key in child_d.keys():
        logger.info("key={}, pid={}".format(key, pid))
        if key==pid:
            family_key=child_d[key]
            break
    logger.info("For pid:{}, Found family_key: {}".format(pid, family_key))

    #find parents for this family
    papaid = None
    mamaid = None
    for key in marriages_d.keys():
        if key==family_key:
            papaid = marriages_d[key][0]
            mamaid = marriages_d[key][1]
            break

    return [papaid, mamaid]


def get_all_codes_pergene(data_permmppcode, kgene):
    """
    """
    codes_l = []
    for code in data_permmppcode.keys():
        _kgene = get_code_gen(code)
        if _kgene==kgene:
            codes_l.append(code)
    
    return codes_l

def get_code_gen(code):
    """
    """
    _kgene = len(code)-4
    if _kgene<0:
        _kgene=0
        
    return _kgene


def find_mmpp_pid(mmpp_tag, Name, Given, birth_year, people_d, verbose_level=1):
    """
    """
    if birth_year == "None":
        birth_year = None

    if birth_year is not None:
        try:
            birth_year = int(birth_year)
        except:
            birth_year = np.NaN
            logger.error("Could not read birth year target")
            return None
    

    if verbose_level>0:
        logger.info("find_mmpp_pid: Name={}".format(Name))
        logger.info("find_mmpp_pid: Given={}".format(Given))
        logger.info("find_mmpp_pid: birth_year={}".format(birth_year))
    
    

    pid = None
    for key in people_d.keys():
        if verbose_level>0:
            logger.info("find_mmpp_pid: _Name={}".format(people_d[key]["name"]))
            logger.info("find_mmpp_pid: _Given={}".format(people_d[key]["given"]))
            logger.info("find_mmpp_pid: _birth_year (STR)={}".format((people_d[key]["birth_year"])))
            #logger.info("_birth_year={}".format(int(people_d[key]["birth_year"])))
        if Name==people_d[key]["name"]:
            if verbose_level>0:
                logger.info("NAME OK")
            if Given==people_d[key]["given"]:
                if verbose_level>0:
                    logger.info("GIVEN OK")
                _birth_year_str = people_d[key]["birth_year"]
                _birth_year = None
                try:
                    _birth_year = int(_birth_year_str)
                except:
                    _birth_year = None
                if (birth_year) is None or (birth_year==_birth_year):
                    if verbose_level>0:
                        logger.info("BIRTH YEAR OK")
                    pid = key
                    break

        #input("find_mmpp_pid LOOP: Press any key...")
    return pid


def read_people(in_csv_fpath, in_type, verbose_level=1):
    """
    """
    loc_type = None
    if in_type == "geneatic_exportgramps":
        loc_type = 1
    elif in_type == "gramps":
        loc_type = 2


    loc_d = read_locations(in_csv_fpath=in_csv_fpath, opt_loc=loc_type)

    people_d = {}
    start_tag_str = "Person,"
    enable_parse = False
    with open(in_csv_fpath, "r") as fin:
        for kline, line in enumerate(fin):
            if verbose_level>0:
                logger.info("reading line : {}".format(line))
            if line.startswith(start_tag_str):
                if verbose_level>0:
                    logger.info("Found start tag in kline:{} line: {}".format(kline, line))
                enable_parse = True
            elif enable_parse and line.startswith("[I"):
                line_split_raw = line.split(",")
                #concatenate items with ""
                line_split = []
                no_cat = True
                for line_item in line_split_raw:
                    line_item = line_item.replace(", ", ",")
                    if no_cat and not ("\"" in line_item):
                        line_split.append(line_item)
                    elif no_cat and (line_item.startswith("\"")) and not line_item.endswith("\""):
                        cat_str = line_item[1:]
                        no_cat = False
                    elif no_cat and (line_item.startswith("\"")) and line_item.endswith("\""):
                        cat_str = line_item[1:-1]
                        line_split.append(cat_str)
                        no_cat = True
                        logger.info("Appending: {}".format(cat_str))
                    elif (not no_cat) and (not ("\"" in line_item)):
                        cat_str = "{},{}".format(cat_str, line_item.strip(" "))
                    elif (not no_cat) and line_item.endswith("\""):
                        cat_str = "{},{}".format(cat_str, line_item[:-1].strip(" "))
                        line_split.append(cat_str)
                        no_cat = True
                
                if verbose_level>0:
                    logger.info("line_split_raw: {}".format(line_split_raw))
                    logger.info("line_split: {}".format(line_split))
                pid = line_split[0][1:-1]
                nom = line_split[1]
                prenom = line_split[2]
                birth_date = line_split[8]
                death_date = line_split[14]
                sexe = line_split[7]

                try:
                    birth_date_split = birth_date.split("-")
                    birth_year = birth_date_split[0]
                except:
                    birth_year = ""

                try:
                    death_date_split = death_date.split("-")
                    death_year = death_date_split[0]
                except:
                    death_year = ""

                try:
                    lid = line_split[9][1:-1]
                    city = loc_d[lid]["city"]
                    country = loc_d[lid]["country"]
                except:
                    city = ""
                    country = ""

                logger.info("pid: {}, prenom:{}, nom:{}, birth_year:{}, death_year:{}, sexe:{}, city:{}, country:{}".format(pid, 
                                    prenom, 
                                    nom, 
                                    birth_year, 
                                    death_year, 
                                    sexe,
                                    city,
                                    country))
                #input()
                people_d[pid] = {"given":prenom, 
                                "name":nom, 
                                "birth_year":birth_year, 
                                "birth_date":birth_date, 
                                "death_year":death_year, 
                                "sexe":sexe,
                                "city":city,
                                "country":country}
            if enable_parse and (not line.startswith("[I")) and (not (line.startswith(start_tag_str))):
                if verbose_level>0:    
                    logger.info("Finished parsing. enable_parse={}".format(enable_parse))
                    logger.info("Finished parsing. (not line.startswith(\"[I\"))={}".format((not line.startswith("[I"))))
                    logger.info("Finished parsing. line=<{}>".format(line))
                    logger.info("Finished parsing. not (line.startswith(start_tag_str)={}".format(not (line.startswith(start_tag_str))))
                    enable_parse = False
                    input("Finished parsing.")


            logger.info("read line : {}".format(line))
            #input()

    return people_d

def read_marriages(in_csv_fpath):
    """
    """

    marriages_d = {}

    start_tag_str = "Marriage,"
    enable_parse = True
    with open(in_csv_fpath, "r") as fin:
        for kline, line in enumerate(fin):
            if line.startswith(start_tag_str):
                logger.info("Found start tag in kline:{} line: {}".format(kline, line))
                enable_parse = True
            elif enable_parse and line.startswith("[F"):
                line_split = line.split(",")
                mid = line_split[0][1:-1]
                ppid = line_split[1][1:-1]
                mpid = line_split[2][1:-1]
                logger.info("mid: {}, ppid:{}, mpid:{}".format(mid, ppid, mpid))
                marriages_d[mid] = [ppid, mpid]
            if enable_parse and (not line.startswith("[F")) and not (line.startswith(start_tag_str)):
                enable_parse = False

    return marriages_d

def read_locations(in_csv_fpath, opt_loc):
    """
    
    Args:
        opt_loc = 1 #gedcom geneanet
        opt_loc = 2 #gedcom gramps
   
    Raises:

    Returns:
        dico[location_id]={}
    """

    
    if opt_loc == 1:
        idx_city = 1
        idx_country = 5 
    elif opt_loc == 2:
        idx_city = 2 
        idx_country = 7

    locations_d = {}
    enable_parse = True
    start_tag_str = "Place,Title,Name,"
    with open(in_csv_fpath, "r") as fin:
        for kline, line in enumerate(fin):
            if line.startswith(start_tag_str):
                logger.info("Found start tag in kline:{} line: {}".format(kline, line))
                enable_parse = True
            elif enable_parse and line.startswith("[P"):
                line_split = line.split(",")
                logger.info("Read line_split: {}".format(line_split))
                lid = line_split[0][1:-1]
                if opt_loc==1:
                    loc_str = line_split[idx_city]
                    logger.info("Read loc_str: {}".format(loc_str))
                    city = loc_str[1:]
                    if "(" in city:
                        city = city.split("(")[0]
                    if "67" in city:
                        city = city.replace(" 67", "")
                    if "57" in city:
                        city = city.replace(" 57", "")

                    if " Nordrhein-Westphalen" in city:
                        city = city.replace(" Nordrhein-Westphalen", "")
                    if "220" in city:
                        city = city.replace("220", "")
                    if "055" in city:
                        city = city.replace("055", "")
                
                    country = line_split[idx_country]
                    ## override country. Ged from geneanet is not clean enough ?
                    if "Allemagne" in str(line):
                        country = "Allemagne"
                    else:
                        logger.info("line: {}".format(line))
                    
                else:
                    line_split_raw = line_split
                    #concatenate items with ""
                    line_split = []
                    no_cat = True
                    for line_item in line_split_raw:
                        line_item = line_item.replace(", ", ",")
                        if no_cat and not ("\"" in line_item):
                            line_split.append(line_item)
                        elif no_cat and (line_item.startswith("\"")):
                            cat_str = line_item[1:]
                            no_cat = False
                        elif no_cat and (line_item.startswith("\"")) and line_item.endswith("\""):
                            cat_str = line_item[1:-1]
                            line_split.append(cat_str)
                            no_cat = True
                        elif (not no_cat) and (not ("\"" in line_item)):
                            cat_str = "{},{}".format(cat_str, line_item.strip(" "))
                        elif (not no_cat) and line_item.endswith("\""):
                            cat_str = "{},{}".format(cat_str, line_item[:-1].strip(" "))
                            line_split.append(cat_str)
                            no_cat = True
                            
                    loc_str = line_split[idx_city]
                    if "," in loc_str:
                        city = loc_str.split(",")[0].strip(" ")
                    else:
                        city = loc_str

                    country = ""
                    if 0: #old data format
                        country_ref = line_split[idx_country][1:-1]
                        try:
                            country = locations_d[country_ref]["city"]
                        except:
                            country = ""
                    if "," in loc_str:
                        country = loc_str.split(",")[-1].rstrip("\n").strip(" ")
                    else:
                        country = loc_str


                logger.info("lid: {}, loc:{}, city:{}, country:{}".format(lid, loc_str, city, country))
                locations_d[lid] = {"str":loc_str, "city":city, "country":country}
            if enable_parse and (not line.startswith("[P")) and not (line.startswith(start_tag_str)):
                enable_parse = False
                
    return locations_d


def read_family_child(in_csv_fpath):
    """
    
    Args:

    Raises:

    Returns:
        dico[child_id]=family_id
    """


    child_d = {}
    start_tag_str = "Family,Child"
    enable_parse = True
    with open(in_csv_fpath, "r") as fin:
        for kline, line in enumerate(fin):
            if line.startswith(start_tag_str):
                logger.info("Found start tag in kline:{} line: {}".format(kline, line))
                enable_parse = True
            elif enable_parse and line.startswith("[F"):
                line_split = line.split(",")
                mid = line_split[0][1:-1]
                chid = line_split[1][1:-2]
                logger.info("mid: {}, chid:{}".format(mid, chid))
                child_d[chid] = mid
            if enable_parse and (not line.startswith("[F")) and not (line.startswith(start_tag_str)):
                enable_parse = False
                
    return child_d


def load_conversion_config(config_fpath):
    """
    """
    col_mapping_d = {"code":0, "surnom":1, "gedcom_nom":2, "gedcom_prenom":3, "gedcom_annee_naissance":4}

    config_d = {}
    if config_fpath is not None:
        with open(config_fpath, "r") as fin:
            for line in fin:
                line = line.lstrip(" ")
                if not line.startswith("#"):
                    line_split = line.split("\t")
                
                    vals_d = {}
                    for key in col_mapping_d.keys():
                        #logger.info("for key={}".format(key))
                        val = (line_split[col_mapping_d[key]]).strip("\n").strip(" ")
                        logger.info("LOAD CONFIG: for key={}, val={}".format(key, val))
                        vals_d[key] = val

                    config_d[vals_d["code"]] = vals_d
                    input("Press any key")
    return config_d


if __name__ == "__main__":
    """
    """

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, description="""
                            convert:
                            """)

    parser.add_argument('-i', action="store", dest="in_csv_fpath", default="./gene_export.csv", help="path to the csv input file")
    parser.add_argument('-c', action="store", dest="in_config_fpath", default="./convert.cfg", help="path to the config input file")
    parser.add_argument('-t', action="store", dest="in_convert_type", default="gramps", help="geneatic_exportgramps, or gramps")
    
    parser.add_argument('-o', action="store", dest="output_fpath", default=None, help="path to the output file")

    args = parser.parse_args()

    if os.path.exists(args.in_csv_fpath):
        in_csv_fpath = os.path.abspath(args.in_csv_fpath)
    else:
        logger.error("input ged file does not exist. aborting")
        exit()

    if os.path.exists(args.in_config_fpath):
        config_fpath = os.path.abspath(args.in_config_fpath)
    else:
        logger.error("input config file does not exist. aborting")
        exit()

        

    process(in_csv_fpath=in_csv_fpath,
            in_type=args.in_convert_type,
            config_fpath=config_fpath,
            output_fpath=args.output_fpath)