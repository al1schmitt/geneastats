"""
display tsv stats
"""

import sys
import os
import numpy as np
import argparse
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from natsort import natsorted
from wordcloud import WordCloud
from matplotlib.lines import Line2D

import folium
from folium.features import CustomIcon
from folium.plugins import MarkerCluster
from geopy.geocoders import Nominatim
import pandas as pd

import seaborn as sns

import pycircos

import time
from selenium import webdriver

#import matplotlib as mpl
#mpl.use('Qt5Agg') #comment if not installed, 


from matplotlib import cm


from pygenestats.common.logger import logger


def process(in_genetsv_fpath,
            output_path,
            in_config_fpath=None):
    """


    Args:

    Raises:

    Returns:

    """

    if output_path is not None and output_path!="None":
        full_output_dir_path = os.path.abspath(output_path)
        if not os.path.isdir(full_output_dir_path):
            logger.info("creating output directory: {}".format(full_output_dir_path))
            os.mkdir(full_output_dir_path)
        else:
            force_new_folder = False
            if force_new_folder:
                logger.error("Output dir already exist. Please delete it and retry: {}".format(full_output_dir_path))
                exit()
            else:
                logger.warning("Output dir already exist. That's ok: {}".format(full_output_dir_path))
                pass
    else:
        full_output_dir_path = None

    
    logger.info("Loading tsv plddt dict")
    tsv_vals = load_tsv_gene(in_tsv_fpath=in_genetsv_fpath)
    #tsv_vals = load_tsv_gene(in_tsv_fpath=in_genetsv_fpath, exclude_mmppcode=["mma", "ppa"])
    vals_percode_d = convert_to_percode_dico(tsv_vals=tsv_vals)

    if 1:
        display_prenom_wordcloud(tsv_vals=tsv_vals, out_dirpath=output_path)
        display_nom_wordcloud(tsv_vals=tsv_vals, out_dirpath=output_path)
        display_metier_wordcloud(tsv_vals=tsv_vals, out_dirpath=output_path)
        display_naissance_ville_wordcloud(tsv_vals=tsv_vals, out_dirpath=output_path)
    
    if 1:
        display_chronolo_annee(tsv_vals=tsv_vals, 
                            out_dirpath=output_path, 
                            periodes={"tag":"Né allemand", "periods":[[1000,1715], [1870,1918]]},
                            in_config_fpath=in_config_fpath)
        
    if 1:
        display_chronolo_periode(tsv_vals=tsv_vals, out_dirpath=output_path, year_start=1914, year_stop=1918, title_tag="Première Guerre Mondiale",
                            in_config_fpath=in_config_fpath)
        display_chronolo_periode(tsv_vals=tsv_vals, out_dirpath=output_path, year_start=1939, year_stop=1945, title_tag="Seconde Guerre Mondiale",
                            in_config_fpath=in_config_fpath)

        display_chronolo_periode(tsv_vals=tsv_vals, out_dirpath=output_path, year_start=1870, year_stop=1871, title_tag="Guerre Franco-Prussienne",
                            in_config_fpath=in_config_fpath)
        display_chronolo_periode(tsv_vals=tsv_vals, out_dirpath=output_path, year_start=1789, year_stop=1799, title_tag="Revolution Française",
                            in_config_fpath=in_config_fpath)


    if 1:
        display_age_naissance_de_lenfant(tsv_vals=tsv_vals, out_dirpath=output_path)
        display_age_deces(tsv_vals=tsv_vals, out_dirpath=output_path)
        display_duree_viecommune_parent_enfant(tsv_vals=tsv_vals, out_dirpath=output_path)
        
    if 1:
        display_circos_prenoms(tsv_vals=tsv_vals, query_list=["Michel", "Michael", "Johannes,Michael", "Johann,Michael"], out_dirpath=output_path,
                            in_config_fpath=in_config_fpath)
        
        display_circos_prenoms(tsv_vals=tsv_vals, query_list=["Georges", 
                                                                "Jean-Georges", 
                                                                "Georg", 
                                                                "Jean,Georges",
                                                                "Johann,Georg"], out_dirpath=output_path,
                            in_config_fpath=in_config_fpath)
        display_circos_prenoms(tsv_vals=tsv_vals, query_list=["Marguerite",
                                                                "Marie,Marguerite"], out_dirpath=output_path,
                            in_config_fpath=in_config_fpath)
        display_circos_prenoms(tsv_vals=tsv_vals, query_list=["Jean", 
                                                                "Jean,Georges",
                                                                "Johann", 
                                                                "Johann,Jacob", 
                                                                "Johannes", 
                                                                "Johannes,Michael",
                                                                "Johann,Michael",
                                                                "Jean,Pierre",
                                                                "Jean,Adam",
                                                                "Johann,Georg"
                                                                ], out_dirpath=output_path,
                            in_config_fpath=in_config_fpath)
        display_circos_prenoms(tsv_vals=tsv_vals, query_list=["Marie", 
                                                                "Maria", 
                                                                "Marie,Marguerite", 
                                                                "Marie,Salomé", 
                                                                "Marie,Catherine", 
                                                                "Anne,Marie", 
                                                                "Anna,Maria",
                                                                "Marthe,Marie",
                                                                "Marie,Marthe"], out_dirpath=output_path,
                            in_config_fpath=in_config_fpath)

        display_circos_prenoms(tsv_vals=tsv_vals, query_list=["Catherine",
                                                            "Marie,Catherine", 
                                                            "Catherina", 
                                                            "Catharina", 
                                                            "Catrine", 
                                                            "Marie,Catherine",
                                                            "Anna,Catherina",
                                                            "Anna,Catharina",
                                                            "Anna,Catherine", 
                                                            "Anna,Katharina",
                                                            "Anna,Katherina",
                                                            "Katarina"], out_dirpath=output_path,
                            in_config_fpath=in_config_fpath)

        display_circos_prenoms(tsv_vals=tsv_vals, query_list=["Anna", 
                                                                "Anne", 
                                                                "Anne,Marie", 
                                                                "Anna,Maria",
                                                                "Anna,Catherina",
                                                                "Anna,Catharina",
                                                                "Anna,Catherine", 
                                                                "Anna,Katharina",
                                                                "Anna,Katherina",
                                                                "Anna,Elisabetha",
                                                                "Anna,Barbara",
                                                                "Anna,Margaretha",
                                                                "Anna,Ursula",
                                                            ], out_dirpath=output_path,
                            in_config_fpath=in_config_fpath)

        display_circos_prenoms(tsv_vals=tsv_vals, query_list=["Emmanuel", 
                                                                "Emmanuel,Marc",
                                                                "Emmanuel,Simon,Michel",
                                                                "Josselin,Georges,Emmanuel",
                                                            ], out_dirpath=output_path,
                            in_config_fpath=in_config_fpath)

        display_circos_prenoms(tsv_vals=tsv_vals, query_list=["Louis",
                                                            "Maurice,Louis",
                                                            "Louis,Victor",
                                                            ], out_dirpath=output_path,
                            in_config_fpath=in_config_fpath)

    if 1:
        display_cities_onmap(tsv_vals=tsv_vals, out_dirpath=output_path)
    


def get_color_bycode(key_code):
    """
    """
    key_mmpp = key_code[:3]
    colors_ppmm_d = {"ppa":"darkblue", "mma":"lightblue", "ppi":"red", "mmi":"orange"}

    return colors_ppmm_d[key_mmpp]


def display_cities_onmap(tsv_vals, out_dirpath=None):
    """
    """
    colors_ppmm_d = {"ppa":"darkblue", "mma":"lightblue", "ppi":"darkred", "mmi":"orange"}

    logger.info("Retrieving all cities and ppmm codes")
    all_cities = []
    all_cities_code = []
    #all_colors_ppmm = []
    for a in tsv_vals:
        code = a["code"]
        if not (code.startswith("ppa") or code.startswith("mma") or code.startswith("ppi") or code.startswith("mmi")):
            continue
        else:
            mmpp_code = code[:3]
            
        #logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        try:
            val_str = (a["naissance_ville"]).strip("\n").strip(" ")
            val2_str = (a["naissance_pays"]).strip("\n").strip(" ")
            if len(val_str)>0:
                all_cities.append("{},{}".format(val_str, val2_str))
                all_cities_code.append("{},{}@{}".format(val_str, val2_str, mmpp_code))
                #all_colors_ppmm.append(colors_ppmm_d[mmpp_code])
        except:
            pass
    
    cities = list(set(all_cities))
    cities_code = list(set(all_cities_code))


    logger.info("Curating cities if not locatable")
    geolocator = Nominatim(user_agent="example")
    #curate cities, remove not locatble
    for k in cities_code:
        city_only = k.split("@")[0]
        geoc = geolocator.geocode(city_only)
        if geoc is None:
            logger.info("WARNING: geolocator cannot find : {}. REMOVING from list".format(k))
            cities_code.remove(k)

    cities_counts = []
    for kc, city in enumerate(cities_code):
        cities_counts.append(all_cities_code.count(city))
    logger.info("cities={}".format(all_cities_code))


    l = {'REGION': cities_code,
        'COUNTS':cities_counts}
    l0 = {'REGION': all_cities}


    logger.info("Building final cities/coordinates data frame")
    l['COORDS'] = []
    for k in l['REGION']:
        city_only = k.split("@")[0]
        geoc = geolocator.geocode(city_only)
        if geoc is not None:
            loc = geoc.raw
            l['COORDS'].append((loc['lat'], loc['lon']))
        else:
            logger.info("cannot find : {}".format(k))
        
    df = pd.DataFrame(l)

    
    loc = geolocator.geocode("Hattmatt").raw
    map_center_point = [loc['lat'], loc['lon']]
    map_zoo = folium.Map(location=map_center_point)#, zoom_start=8)
    #map_zoo = folium.Map()


    logger.info("Creating folium map")
    folium.TileLayer('cartodbpositron').add_to(map_zoo)
    marker_cluster = MarkerCluster().add_to(map_zoo)
    for i,r in df.iterrows():
        #folium.Marker(location=r['COORDS'],
        #            popup = r['REGION'],
        #            tooltip="{:.0f}".format(l['COUNTS'][i])).add_to(marker_cluster)

        ppmm_code = l['REGION'][i].split("@")[1]
        color = colors_ppmm_d[ppmm_code]
        folium.CircleMarker(
                            location=r['COORDS'],
                            radius = float(np.sqrt(l['COUNTS'][i])*5),
                            #popup="Population 2011 : %s"%state_census["Population in 2011"].values[0],
                            tooltip="{} : {:.0f}".format(l['REGION'][i], l['COUNTS'][i]),
                            color=color,
                            fill_color=color
                        ).add_to(map_zoo)
                        
        #folium.Marker(location=r['COORDS'],
        #            popup = r['REGION']).add_to(marker_cluster)

    sw = df[['COORDS']].min().values.tolist()
    ne = df[['COORDS']].max().values.tolist()

    map_zoo.fit_bounds([sw, ne])


    if out_dirpath is None:
        map_zoo.show_in_browser()
    else:
        out_fpath = os.path.join(out_dirpath, "carte_ville_naissance.png")
        delay=5
        fn='testmap.html'
        tmpurl='file://{path}/{mapfile}'.format(path=os.getcwd(),mapfile=fn)
        map_zoo.save(fn)

        browser = webdriver.Firefox()
        browser.get(tmpurl)
        #Give the map tiles some time to load
        time.sleep(delay)
        browser.save_screenshot(out_fpath)
        browser.quit()
        bashcommand = "rm {}".format(fn)
        os.system(bashcommand)


def display_circos_prenoms(tsv_vals, query_list, out_dirpath=None, in_config_fpath=None):
    """
    """
    
    labels_ppmm_d = get_labels_ppmm_d(in_config_fpath=in_config_fpath)

    info_per_generation = {}
    
    all_generations = []
    for a in tsv_vals:
        code = a["code"]
        if not (code.startswith("ppa") or code.startswith("mma") or code.startswith("ppi") or code.startswith("mmi")):
            continue
            
        #logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        age_deces = None
        val_prenom = None
        try:
            val_prenom = a["prenom"]
            val_nom = a["nom"]
        except:
            logger.info("cannot load a={}".format(a))
            continue

        if val_prenom is not None:
            generation = len(code)-4
            if generation==-1:
                generation = 0
                if code.startswith("m"):
                    sexe = "femme"
                else:
                    sexe = "homme"
            else:
                if code.endswith("m"):
                    sexe = "femme"
                else:
                    sexe = "homme"

            all_generations.append(generation)
            #logger.info("for code={}, generation={}".format(code, generation))
            #logger.info("age_deces: {}".format(age_deces))

            info_d = {"prenom":val_prenom, "nom":val_nom, "code":a["code"]}

            if not generation in info_per_generation.keys():
                info_per_generation[generation] = []
            info_per_generation[generation].append(info_d)
            
    # preparing generation sizes list
    generation_sizes = []
    gene_keys = sorted(info_per_generation.keys())
    for info_g_key in gene_keys:
        length=len(info_per_generation[info_g_key])
        generation_sizes.append(length)

    # preparing prenom links
    #STEP1: find most populated same family name
    prenom_links_d = get_prenom_links(gene_keys=gene_keys,
                                info_per_generation=info_per_generation,
                                query_list=query_list,
                                enable_unique_query=True,
                                enable_genelevel=30)
    links_l = prenom_links_d["links"]
    name_same = prenom_links_d["name_same"] 
    name_other = prenom_links_d["name_other"]
    logger.info("Found links shits N={}".format(len(links_l)))

    count, high_count_family_name = get_highest_count_name(name_same, name_other)
    if count is None or count<2:
        high_count_family_name = None
    logger.info("Get links with highest count family name: {}".format(high_count_family_name))
    #input("press any key")

    #STEP2: get links wrt highest count family name
    prenom_links_d = get_samebranch_links(gene_keys=gene_keys,
                                info_per_generation=info_per_generation,
                                query_list=query_list,
                                selected_family_name=None,#high_count_family_name,
                                enable_unique_query=False,
                                enable_genelevel=100)
    links_l = prenom_links_d["links"]

    #redo for name_same, name_other count
    prenom_links_d = get_samebranch_links(gene_keys=gene_keys,
                                info_per_generation=info_per_generation,
                                query_list=query_list,
                                selected_family_name=high_count_family_name,
                                enable_unique_query=True,
                                enable_genelevel=100)
    name_same = prenom_links_d["name_same"] 
    name_other = prenom_links_d["name_other"]
    logger.info("Found links shits N={}".format(len(links_l)))


    #list the family names and count
    name_same_set = list(set(name_same))
    name_same_str = str(name_same_set)
    if len(name_same_set)>0:
        n_same_name = name_same.count(name_same_set[0])
    else:
        n_same_name = 0

    nchr_max = 110
    name_other_set = list(set(name_other))
    name_other_str = ""
    name_other_count_d = {}
    count_line = 0
    for kn, name in enumerate(name_other_set):
        name_other_count_d[name] = name_other.count(name)
        if count_line>0:
            name_other_str = "{}, {}({})".format(name_other_str, name, name_other_count_d[name])
        else:
            name_other_str = "{}{}({})".format(name_other_str, name, name_other_count_d[name])
        
        nchr_lastline = len(name_other_str.split("\n")[-1])
        if nchr_lastline>nchr_max:#count_line>=9:
            name_other_str = "{}\n".format(name_other_str)
            count_line = 0
        count_line += 1




    df = pd.DataFrame(data=np.array(generation_sizes),
                  index=["{}".format(a) for a in list(gene_keys)],
                  columns=["length"])
    logger.info("df={}".format(df))

    cg = pycircos.Circos(df, gap=4, figsize=(12.5, 12.5))
    
    #cg.draw_scaffold(8.1, 0.3)
    if 0:
        cg.draw_ticks(8.1, 0.2, inside=True)
    else:
        #draw tick manually
        tick_gap=500000000
        for i, gid in enumerate(cg.regions.index):
            et1, et2 = cg.regions.offset[gid], cg.get_theta([gid], [cg.regions.length[gid] - 1])
            ets = np.arange(et1, et2, tick_gap / cg.len_per_theta)
            inside = True
            if inside:
                if i==0:
                    tick_length = 3
                    rad = 11.75
                else:
                    tick_length = 0.75
                    rad = 9.5

                cg.pax.vlines(ets, [rad] * len(ets), [rad - tick_length] * len(ets), 
                                color="black", 
                                linestyle="dashed", 
                                alpha=0.4)
            else:
                cg.pax.vlines(ets, [rad] * len(ets), [rad + tick_length] * len(ets))

    # text num generation
    rad = 9.75
    ml = max([len(gid) for gid in cg.regions.index])
    for kgid, gid in enumerate(cg.regions.index):
        deg = np.rad2deg(cg.get_theta(gid, cg.regions.length[gid] / 2))
        if inside:  # add spaces to the right side
            lstr = ' ' * (ml - len(gid)) + gid
        else:
            lstr = gid + ' ' * (ml - len(gid))
        gid_tag = gid
        #if gid=="4" or gid==4:
        #    gid_tag = "generation: {}".format(gid)
        cg.pax.annotate("{}".format(gid_tag), 
                        xy=[np.deg2rad(deg), rad], 
                        ha='center', 
                        va='center', fontsize=14)#, rotation=rotation+deg)

    # text count per generation
    rad = 9
    ml = max([len(gid) for gid in cg.regions.index])
    for kgid, gid in enumerate(cg.regions.index):
        deg = np.rad2deg(cg.get_theta(gid, cg.regions.length[gid] / 2))
        if inside:  # add spaces to the right side
            lstr = ' ' * (ml - len(gid)) + gid
        else:
            lstr = gid + ' ' * (ml - len(gid))
        cg.pax.annotate("({})".format(generation_sizes[kgid]), 
                        xy=[np.deg2rad(deg), rad], 
                        ha='center', 
                        va='center')#, rotation=rotation+deg)
        
    cg.pax.set_xlabel("Prénom : {}".format(query_list[0]))

    #draw mmpp color circle
    colors_ppmm_d = {"ppa":"darkblue", "mma":"lightblue", "ppi":"red", "mmi":"orange"}
    bottom_rad = 7.5
    width_rad = None
    for i, gid in enumerate(cg.regions.index):
        #if i!=0:
        #    continue
        if width_rad is None and i==0:
            width_rad = cg.get_theta([gid], [1])
            
        et1, et2 = cg.regions.offset[gid], cg.get_theta([gid], [cg.regions.length[gid] - 1])

        info_g_key = gene_keys[i]
        for ki, info_g in enumerate(info_per_generation[info_g_key]):
            color = get_color_bycode(key_code=info_g["code"])
            
            #logger.info("cg.regions.length[gid]={}".format(cg.regions.length[gid]))
            start_angle = cg.regions.offset[gid] + 0.5*width_rad[0] + ki*width_rad[0]
            
            #logger.info("start_angle={}".format(start_angle))
            #logger.info("width_rad={}".format(width_rad))
            cg.pax.bar(start_angle, 1., width=width_rad, bottom=bottom_rad, color=color)


    #draw prenom color circle
    selected_family_name = high_count_family_name
    color_same_prenom_same_nom = "limegreen"
    color_same_prenom_other_nom = "gray"
    color_other_prenom = "white"  
    bottom_rad = 6.5
    width_rad = None
    for i, gid in enumerate(cg.regions.index):
        #if i!=0:
        #    continue
        if width_rad is None and i==0:
            width_rad = cg.get_theta([gid], [1])
            
        et1, et2 = cg.regions.offset[gid], cg.get_theta([gid], [cg.regions.length[gid] - 1])

        info_g_key = gene_keys[i]
        for ki, info_g in enumerate(info_per_generation[info_g_key]):
            
            query_prenom = info_g["prenom"].replace(" ", "")
            query_nom = info_g["nom"].replace(" ", "")
            if (query_list is not None) and (not (query_prenom in query_list)):
                color = color_other_prenom
            else:
                if (selected_family_name is not None) and not (selected_family_name == query_nom):
                    color = color_same_prenom_other_nom
                elif selected_family_name is None:
                    color = color_same_prenom_other_nom
                else:
                    color = color_same_prenom_same_nom
            
            #logger.info("cg.regions.length[gid]={}".format(cg.regions.length[gid]))
            start_angle = cg.regions.offset[gid] + 0.5*width_rad[0] + ki*width_rad[0]
            
            #logger.info("start_angle={}".format(start_angle))
            #logger.info("width_rad={}".format(width_rad))
            cg.pax.bar(start_angle, 0.75, width=width_rad, bottom=bottom_rad, color=color)


    #Draw links
    color_link_same_branch = "lightgray"
    alpha_same_nom = 0.3
    half_thinckness_same_nom = 0.2

    color_link_same_branch_nextgen = "fuchsia"
    alpha_same_nom_nextgen = 0.8
    half_thinckness_same_nom_nextgen = 0.4

    color_link_default = "black"
    alpha_default = 0.1
    half_thinckness_default = 0.05
    
    links_rad = 6.25
    for link in links_l:
        if link["level"]==1:
            ngen = np.abs(int(link["query"]["g_key"])-int(link["hit"]["g_key"]))
            if ngen<=1:
                color = color_link_same_branch_nextgen
                thickness = half_thinckness_same_nom_nextgen
                alpha = alpha_same_nom_nextgen
            else:
                color = color_link_same_branch
                thickness = half_thinckness_same_nom
                alpha = alpha_same_nom
        else:
            continue
            color = color_link_default
            alpha = alpha_default
            thickness = half_thinckness_default
        cg.draw_link(links_rad, 
                    ["{}".format(link["query"]["g_key"]), "{}".format(link["hit"]["g_key"])], 
                    [link["query"]["ki"]+0.5-thickness, link["hit"]["ki"]+0.5-thickness], 
                    [link["query"]["ki"]+0.5+thickness, link["hit"]["ki"]+0.5+thickness], 
                    #[width_rad, width_rad], 
                    color=color, alpha=alpha)

    title_patch = mpatches.Patch(color='black', alpha=0.0, label='>{}'.format(query_list[0]))


    blue_patch = mpatches.Patch(color='darkblue', label=labels_ppmm_d['ppa'])
    lightnlue_patch = mpatches.Patch(color='lightblue', label=labels_ppmm_d['mma'])
    red_patch = mpatches.Patch(color='red', label=labels_ppmm_d['ppi'])
    orange_patch = mpatches.Patch(color='orange', label=labels_ppmm_d['mmi'])
    
    legend1 = plt.legend(frameon=False,
                    loc='upper right', 
                    handles=[blue_patch,
                             lightnlue_patch,
                             red_patch,
                             orange_patch   
                                ], 
                    ncol=1,
                    fontsize=12)
    plt.gca().add_artist(legend1)

    if n_same_name==0:        
        name_same_int_final = 0
    else:
        name_same_int_final = n_same_name+1
    color_same_nom = color_same_prenom_same_nom
    name_same_patch = mpatches.Patch(color=color_same_nom, alpha=1, label='Nom de Famille le plus fréquent\n{} ({})'.format(name_same_str, name_same_int_final))
    name_other_patch = mpatches.Patch(color='black', alpha=0.1, label='Autres Noms de Famille\n{}'.format(name_other_str))
    
    branch_same_patch = mpatches.Patch(color=color_link_same_branch, alpha=1, label="Même branche")
    
    branch_same_line = Line2D([0], [0], label='lien même branche', color=color_link_same_branch)
    branch_same_line_nextgen = Line2D([0], [0], label='lien même branche, génération consécutive', color=color_link_same_branch_nextgen)

    sep_patch = mpatches.Patch(color='black', alpha=0.0, label=' ')

    plt.legend(frameon=False,
                    loc='lower left', 
                    handles=[branch_same_line,
                                branch_same_line_nextgen,
                                #blue_patch, 
                                #lightnlue_patch, 
                                #red_patch, 
                                #orange_patch, 
                                #sep_patch, 
                                name_same_patch, 
                                name_other_patch,
                                #sep_patch,
                                ], 
                    ncol=1,
                    fontsize=12)
                    
    plt.text(3*np.pi/4, 
            16, 
            "Prénom : {}".format(query_list[0]), 
            ha="left", 
            va="center", 
            fontsize=24, 
            color=color_same_nom)


    if out_dirpath is not None:
        if not os.path.isdir(out_dirpath):
            os.mkdir(out_dirpath)
        out_fpath = os.path.join(out_dirpath, "circos_prenom_{}.png".format(query_list[0]))
        plt.savefig(out_fpath, dpi=150)
    else:
        plt.show()

def get_highest_count_name(name_same, name_other):
    """
    """
    name_count_d = {}

    #list the family names and count
    name_same_set = list(set(name_same))
    if len(name_same_set)>0:
        name_count_d[name_same[0]] = name_same.count(name_same_set[0])
    

    name_other_set = list(set(name_other))
    for kn, name in enumerate(name_other_set):
        if name in name_count_d.keys():
            prev_count = name_count_d[name]
        else:
            prev_count = 0
        name_count_d[name] = name_other.count(name) + prev_count
        
    high_count_family_name = None
    high_count = None
    for key in name_count_d.keys():
        count = name_count_d[key]
        if high_count is None or count>high_count:
            high_count = count
            high_count_family_name = key


    return high_count, high_count_family_name


def get_prenom_links(gene_keys,
                    info_per_generation,
                    query_list,
                    selected_family_name=None,
                    enable_unique_query=True,
                    enable_genelevel=None):
    """
    """

    
    query_unique_l = []
    links_l = []
    name_same = []
    name_other = []
    info_pergen_perk = {}
    for info_g_key in gene_keys[::-1]:
        for ki, info_g in enumerate(info_per_generation[info_g_key]):
            query_prenom = info_g["prenom"].replace(" ", "")
            query_nom = info_g["nom"].replace(" ", "")
            if (query_list is not None) and (not (query_prenom in query_list)):
                continue
            if (selected_family_name is not None) and not (selected_family_name == query_nom):
                continue 
            
            #if len(name_same)==0:
            #    name_same.append(query_nom)

            if query_prenom in query_unique_l:
                continue
            else:
                if enable_unique_query:
                    query_unique_l.append(query_prenom)
                    for query_str in query_list:
                        query_unique_l.append(query_str)
                query_dico = {"g_key":info_g_key, "ki":ki}

                for info_g_key2 in gene_keys:

                    if enable_genelevel is not None:
                        if np.abs(info_g_key-info_g_key2)>enable_genelevel:
                            continue

                    for ki2, info_g2 in enumerate(info_per_generation[info_g_key2]):
                        hit_prenom = info_g2["prenom"].replace(" ", "")
                        hit_nom = info_g2["nom"].replace(" ", "")
                        if (hit_prenom in query_list) and not (ki==ki2 and info_g_key==info_g_key2):
                            hit_dico = {"g_key":info_g_key2, "ki":ki2}
                            level = int( (info_g["nom"]==info_g2["nom"]) )
                            if level==1 and (selected_family_name is not None):
                                if selected_family_name !=info_g2["nom"]:
                                    level = 0
                            if level==1:
                                name_same.append(hit_nom)
                            else:
                                name_other.append(hit_nom)
                            links_item = {"query":query_dico, "hit":hit_dico, "level":level}
                            logger.info("links_item={}".format(links_item))
                            logger.info("query_prenom={}, hit_prenom={}, hit_nom={}".format(query_prenom, hit_prenom, hit_nom))
                            links_l.append(links_item)

    ret_d = {}
    ret_d["links"] = links_l
    ret_d["name_same"] = name_same
    ret_d["name_other"] = name_other
    return ret_d



def get_samebranch_links(gene_keys,
                    info_per_generation,
                    query_list,
                    selected_family_name=None,
                    enable_unique_query=True,
                    enable_genelevel=None):
    """
    """

    
    query_unique_l = []
    links_l = []
    name_same = []
    name_other = []
    info_pergen_perk = {}
    for info_g_key in gene_keys[::-1]:
        for ki, info_g in enumerate(info_per_generation[info_g_key]):
            query_prenom = info_g["prenom"].replace(" ", "")
            query_nom = info_g["nom"].replace(" ", "")
            if (query_list is not None) and (not (query_prenom in query_list)):
                continue
            if (selected_family_name is not None) and not (selected_family_name == query_nom):
                continue 
            
            #if len(name_same)==0:
            #    name_same.append(query_nom)

            if query_prenom in query_unique_l:
                continue
            else:
                if enable_unique_query:
                    query_unique_l.append(query_prenom)
                    for query_str in query_list:
                        query_unique_l.append(query_str)
                query_dico = {"g_key":info_g_key, "ki":ki}

                for info_g_key2 in gene_keys:

                    if enable_genelevel is not None:
                        if np.abs(info_g_key-info_g_key2)>enable_genelevel:
                            continue

                    for ki2, info_g2 in enumerate(info_per_generation[info_g_key2]):
                        hit_prenom = info_g2["prenom"].replace(" ", "")
                        hit_nom = info_g2["nom"].replace(" ", "")
                        if (hit_prenom in query_list) and not (ki==ki2 and info_g_key==info_g_key2):
                            hit_dico = {"g_key":info_g_key2, "ki":ki2}
                            level = int( (info_g["nom"]==info_g2["nom"]) )
                            if level==1 and (selected_family_name is not None):
                                if selected_family_name !=info_g2["nom"]:
                                    level = 0
                            if level==1:
                                name_same.append(hit_nom)
                            else:
                                name_other.append(hit_nom)

                            if is_same_branch(info_g["code"], info_g2["code"]):
                                level=1
                            else:
                                level=0
                            
                            links_item = {"query":query_dico, "hit":hit_dico, "level":level}
                            logger.info("links_item={}".format(links_item))
                            logger.info("query_prenom={}, hit_prenom={}, hit_nom={}".format(query_prenom, hit_prenom, hit_nom))
                            links_l.append(links_item)

    ret_d = {}
    ret_d["links"] = links_l
    ret_d["name_same"] = name_same
    ret_d["name_other"] = name_other
    return ret_d

def is_same_branch(code1, code2):
    """
    """
    smallest_code = None
    if len(code1)==len(code2):
        if code1==code2:
            return True
        else:
            return False
    if len(code1)<len(code2):
        smallest_code = code1
        longest_code = code2
    else:
        smallest_code = code2
        longest_code = code1

    if longest_code.startswith(smallest_code):
        return True
    else:
        return False
    


def display_duree_viecommune_parent_enfant(tsv_vals, out_dirpath=None):
    """
    """
    duree_per_generation_persexe = {}
    duree_per_generation_persexe["homme"]={}
    duree_per_generation_persexe["femme"]={}
    all_duree_avec_enfant = []
    all_generations = []
    for a in tsv_vals:
        code = a["code"]
        if not (code.startswith("ppa") or code.startswith("mma") or code.startswith("ppi") or code.startswith("mmi")):
            continue
            
        logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        try:
            val_int = int(a["naissance_year"])
        except:
            continue

        deces_parent = get_info_parent(tsv_vals=tsv_vals, code=code, key_info="deces_year", type="int")
        #if code=="ppi":
        #    stop
        for year_code_d in deces_parent:
            generation = len(year_code_d["code"])-4
            all_generations.append(generation)
            logger.info("for code={}, generation={}".format(year_code_d["code"], generation))
            
            duree_avec_enfant =  year_code_d["year"] - val_int
            all_duree_avec_enfant.append(duree_avec_enfant)
            logger.info("duree_avec_enfant: {}".format(duree_avec_enfant))

            if year_code_d["code"].endswith("m"):
                sexe = "femme"
            else:
                sexe = "homme"

            if not generation in duree_per_generation_persexe[sexe].keys():
                duree_per_generation_persexe[sexe][generation] = []
            duree_per_generation_persexe[sexe][generation].append({"duree":duree_avec_enfant, "code":year_code_d["code"]})
            logger.info("duree_per_generation: {}".format(duree_per_generation_persexe[sexe]))


    logger.info("all_duree_avec_enfant: {}".format(all_duree_avec_enfant))
    
    sns.set_theme()
    sns.set_style("white")
    #sns.set_context("talk")
    fig = plt.figure(figsize=(12, 6))
    ax1 = plt.subplot(1, 1, 1)
    #xval = range(len(list(maxconfidence_d.keys())))
    #xval = [msa_max_nover25_d[key] for key in maxconfidence_d.keys()]
    #yval = [maxconfidence_d[key] for key in maxconfidence_d.keys()]
    
    for sexe in ["homme", "femme"]:
        if sexe=="homme":
            color="blue"
        else:
            color="red"
        avg_ages_yvals = []
        avg_ages_yerrs = []
        avg_gen_xvals = []
        for keygen in natsorted(duree_per_generation_persexe[sexe].keys()):
            xvals = np.ones(len(duree_per_generation_persexe[sexe][keygen]))*int(keygen)
            yvals = [a["duree"] for a in duree_per_generation_persexe[sexe][keygen]]

            colors = [get_color_bycode(key_code=a["code"]) for a in duree_per_generation_persexe[sexe][keygen]]

            for kx in range(len(xvals)):
                ax1.plot(xvals[kx], yvals[kx], "x", color=colors[kx])
            avg_gen_xvals.append(np.mean(xvals))
            avg_ages_yvals.append(np.mean(yvals))
            avg_ages_yerrs.append(np.std(yvals))


        ax1.errorbar(avg_gen_xvals, avg_ages_yvals, yerr=avg_ages_yerrs, color=color, label="Moyenne", alpha=0.3)
    #ax1.set_title('Age du parent à la naissance de l\'enfant\n')
    ax1.set_xlabel("Génération du parent (grand-parent + k)\n")
    #ax1.set_xscale('log')
    ax1.set_ylabel("Durée de vie avec l\'enfant\n(Années)")
    plt.minorticks_on()
    plt.grid(b=True, which='major', linewidth=.5)
    #plt.grid(b=True, which='minor', linewidth=0.1)
    if len(all_generations)<1:
        return
    major_ticks = np.arange(np.min(all_generations)-1, np.max(all_generations)+1)
    ax1.set_xticks(major_ticks)

    ax1.set_xlim([np.min(all_generations)-0.5, np.max(all_generations)+.5])

    red_patch = mpatches.Patch(color='red', label='Femme')
    blue_patch = mpatches.Patch(color='blue', label='Homme')
    plt.legend(handles=[red_patch, blue_patch], ncol=2)

    sns.despine(offset=10, trim=True)
    plt.tight_layout()

    if out_dirpath is not None:
        if not os.path.isdir(out_dirpath):
            os.mkdir(out_dirpath)
        out_fpath = os.path.join(out_dirpath, "duree_vie_avec_enfant.png")
        plt.savefig(out_fpath, dpi=300)
    else:
        plt.show()


def display_age_naissance_de_lenfant(tsv_vals, out_dirpath=None):
    """
    """
    age_per_generation_persexe = {}
    age_per_generation_persexe["homme"]={}
    age_per_generation_persexe["femme"]={}
    all_age_naissance_enfant = []
    all_generations = []
    for a in tsv_vals:
        code = a["code"]
        if not (code.startswith("ppa") or code.startswith("mma") or code.startswith("ppi") or code.startswith("mmi")):
            continue
            
        logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        try:
            val_int = int(a["naissance_year"])
        except:
            continue

        naissance_parent = get_info_parent(tsv_vals=tsv_vals, code=code, key_info="naissance_year", type="int")
        for year_code_d in naissance_parent:
            generation = len(year_code_d["code"])-4
            all_generations.append(generation)
            logger.info("for code={}, generation={}".format(year_code_d["code"], generation))
            age_naissance_enfant = val_int - year_code_d["year"]
            all_age_naissance_enfant.append(age_naissance_enfant)
            logger.info("age_naissance_enfant: {}".format(age_naissance_enfant))

            if year_code_d["code"].endswith("m"):
                sexe = "femme"
            else:
                sexe = "homme"

            if not generation in age_per_generation_persexe[sexe].keys():
                age_per_generation_persexe[sexe][generation] = []
            age_per_generation_persexe[sexe][generation].append(age_naissance_enfant)
            
    logger.info("all age_naissance_enfant: {}".format(all_age_naissance_enfant))
    logger.info("age_per_generation: {}".format(age_per_generation_persexe[sexe]))

    sns.set_theme()
    sns.set_style("white")
    #sns.set_context("talk")
    fig = plt.figure(figsize=(12, 6))
    ax1 = plt.subplot(1, 1, 1)
    #xval = range(len(list(maxconfidence_d.keys())))
    #xval = [msa_max_nover25_d[key] for key in maxconfidence_d.keys()]
    #yval = [maxconfidence_d[key] for key in maxconfidence_d.keys()]
    
    for sexe in ["homme", "femme"]:
        if sexe=="homme":
            color="blue"
        else:
            color="red"
        avg_ages_yvals = []
        avg_ages_yerrs = []
        avg_gen_xvals = []
        for keygen in natsorted(age_per_generation_persexe[sexe].keys()):
            xvals = np.ones(len(age_per_generation_persexe[sexe][keygen]))*int(keygen)
            yvals = age_per_generation_persexe[sexe][keygen]
            ax1.plot(xvals, yvals, "x", color=color)
            avg_gen_xvals.append(np.mean(xvals))
            avg_ages_yvals.append(np.mean(yvals))
            avg_ages_yerrs.append(np.std(yvals))
        ax1.errorbar(avg_gen_xvals, avg_ages_yvals, yerr=avg_ages_yerrs, color=color, label="Moyenne")
    #ax1.set_title('Age du parent à la naissance de l\'enfant\n')
    ax1.set_xlabel("Génération du parent (grand-parent + k)\n")
    #ax1.set_xscale('log')
    ax1.set_ylabel("Age du parent à la naissance de l\'enfant\n(Années)")
    plt.minorticks_on()
    plt.grid(b=True, which='major', linewidth=.5)
    #plt.grid(b=True, which='minor', linewidth=0.1)
    major_ticks = np.arange(np.min(all_generations)-1, np.max(all_generations)+1)
    ax1.set_xticks(major_ticks)

    ax1.set_xlim([np.min(all_generations)-0.5, np.max(all_generations)+.5])

    red_patch = mpatches.Patch(color='red', label='Femme')
    blue_patch = mpatches.Patch(color='blue', label='Homme')
    plt.legend(handles=[red_patch, blue_patch], ncol=2)

    sns.despine(offset=10, trim=True)
    plt.tight_layout()

    if out_dirpath is not None:
        if not os.path.isdir(out_dirpath):
            os.mkdir(out_dirpath)
        out_fpath = os.path.join(out_dirpath, "age_parents_ala_naissance.png")
        plt.savefig(out_fpath, dpi=300)
    else:
        plt.show()

def get_labels_ppmm_d(in_config_fpath):
    """
    """
    col_mapping_d = {"code":0, "surnom":1, "gedcom_nom":2, "gedcom_prenom":3, "gedcom_annee_naissance":4}

    labels_ppmm_d = {}
    if in_config_fpath is not None:
        with open(in_config_fpath, "r") as fin:
            for line in fin:
                line = line.lstrip(" ")
                if not line.startswith("#"):
                    line_split = line.split("\t")
                
                    vals_d = {}
                    for key in col_mapping_d.keys():
                        #logger.info("for key={}".format(key))
                        val = (line_split[col_mapping_d[key]]).strip("\n").strip(" ")
                        logger.info("LOAD CONFIG: for key={}, val={}".format(key, val))
                        vals_d[key] = val

                    labels_ppmm_d[vals_d["code"]] = vals_d["surnom"]
                    #input("Press any key")
    else:
        labels_ppmm_d = {"ppa":"grandpa1", "mma":"grandma1", "ppi":"grandpa2", "mmi":"grandma2"}

    return labels_ppmm_d


def display_chronolo_annee(tsv_vals, 
                            out_dirpath=None, 
                            type="naissance", 
                            periodes=None, 
                            in_config_fpath=None):
    """

    Args: 
        type: "naissance", "deces"

    Raises:

    Returns:

    """
    labels_ppmm_d = get_labels_ppmm_d(in_config_fpath=in_config_fpath)
    colors_ppmm_d = {"ppa":"darkblue", "mma":"lightblue", "ppi":"red", "mmi":"orange"}
    mmpp_code_geneoffset = {"ppa":0.04, "mma":0.02, "ppi":-0.02, "mmi":-0.04}

    logger.info("Retrieving all annees and ppmm codes")
    all_annees = []
    all_annees_code = []
    #all_colors_ppmm = []
    for a in tsv_vals:
        code = a["code"]
        if not (code.startswith("ppa") or code.startswith("mma") or code.startswith("ppi") or code.startswith("mmi")):
            continue
        else:
            mmpp_code = code
            
        #logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        try:
            val_str = (a["naissance_year"]).strip("\n").strip(" ")
            if len(val_str)>0:
                all_annees.append(int(val_str))
                all_annees_code.append("{}@{}".format(val_str, mmpp_code))
                #all_colors_ppmm.append(colors_ppmm_d[mmpp_code])
        except:
            pass


    all_generations = []
    fig = plt.figure(figsize=(12, 6))
    ax1 = plt.subplot(1, 1, 1)
    
    sns.set_theme()
    sns.set_style("white")


    min_annees = None
    max_annees = None

    logger.info("all_annees: {}".format(all_annees))
    bins = list(np.linspace(1700, 2030, 2030-1700+1))
    for key in colors_ppmm_d.keys():
        mmpp_annees = []
        mmpp_generation_k = []
        sexe_marker_l = []
        for ka in range(len(all_annees)):
            rawcode = all_annees_code[ka]
            code = rawcode.split("@")[1]
            if code.startswith(key):
                mmpp_annees.append(all_annees[ka])
                gene_k = len(code)-4
                if gene_k==-1:
                    gene_k = 0
                    if code.startswith("m"):
                        sexe_marker = "+"
                    else:
                        sexe_marker = "o"
                else:
                    if code.endswith("m"):
                        sexe_marker = "+"
                    else:
                        sexe_marker = "o"
                    
                mmpp_generation_k.append(gene_k+mmpp_code_geneoffset[key])
                all_generations.append(gene_k)
                sexe_marker_l.append(sexe_marker)
                
        if len(mmpp_annees)>0:
            min_a = np.min(mmpp_annees)
            max_a = np.max(mmpp_annees)
            if min_annees is None or min_a<min_annees:
                min_annees = min_a
            if max_annees is None or max_a>max_annees:
                max_annees = max_a

        logger.info("mmpp_annees: n={}, {}".format(len(mmpp_annees), mmpp_annees))
        logger.info("mmpp_generation_k: n={}, {}".format(len(mmpp_generation_k),mmpp_generation_k))
        for km in range(len(mmpp_annees)):
            if sexe_marker_l[km]=="+":
                marker_size = 12
                alpha = 0.9
            else:
                marker_size = 8
                alpha = 0.6
            ax1.plot(mmpp_annees[km], mmpp_generation_k[km], marker=sexe_marker_l[km], markersize=marker_size, color=colors_ppmm_d[key], alpha=alpha)

    logger.info("min_annees={}".format(min_annees))
    logger.info("max_annees={}".format(max_annees))

    #display periods in gray
    max_gen = np.max(all_generations)+1
    for per in periodes["periods"]:
        ax1.fill_between([per[0], per[1]], [0, 0], [max_gen, max_gen], color="black", edgecolor="white", alpha=0.1, )
    #tag
    ax1.set_xlim([min_annees-20, 1940])
    ax1.set_ylim([-0.5, max_gen+1])

    sns.despine(offset=10, trim=True)

    blue_patch = mpatches.Patch(color='darkblue', label=labels_ppmm_d['ppa'])
    lightnlue_patch = mpatches.Patch(color='lightblue', label=labels_ppmm_d['mma'])
    man_patch = mpatches.Patch(color='black', alpha=0.0, label='o Homme')
    woman_patch = mpatches.Patch(color='black', alpha=0.0, label='+ Femme')
    sep_patch = mpatches.Patch(color='black', alpha=0.0, label=' ')
    red_patch = mpatches.Patch(color='red', label=labels_ppmm_d['ppi'])
    orange_patch = mpatches.Patch(color='orange', label=labels_ppmm_d['mmi'])
    gray_patch = mpatches.Patch(color='gray', alpha=0.2, label='Né allemand')
    plt.legend(frameon=False,
                    loc='upper center', 
                    handles=[blue_patch, lightnlue_patch, red_patch, orange_patch, man_patch, woman_patch, gray_patch], 
                    ncol=7)

    ax1.set_ylabel("Génération (grand-parent + k)\n")
    ax1.set_xlabel("Année de naissance\n")

    #plt.minorticks_on()
    #plt.grid(b=True, which='major', linewidth=.5)
    #plt.grid(b=True, which='minor', linewidth=0.1)
    #major_ticks = np.arange(np.min(all_generations)-1, np.max(all_generations)+1)
    #ax1.set_yticks(major_ticks)



    if out_dirpath is not None:
        if not os.path.isdir(out_dirpath):
            os.mkdir(out_dirpath)
        out_fpath = os.path.join(out_dirpath, "chronolo_annee_naissance.png")
        plt.savefig(out_fpath, dpi=300)
    else:
        plt.show()


def display_chronolo_periode(tsv_vals, 
                                out_dirpath=None, 
                                year_start=1914, 
                                year_stop=1918, 
                                title_tag="Première guerre mondiale",
                                in_config_fpath=None):
    """

    Args: 
        type: "naissance", "deces"

    Raises:

    Returns:

    """
    labels_ppmm_d = get_labels_ppmm_d(in_config_fpath=in_config_fpath)
    colors_ppmm_d = {"ppa":"darkblue", "mma":"lightblue", "ppi":"red", "mmi":"orange"}
    mmpp_code_geneoffset = {"ppa":0.2, "mma":0.1, "ppi":-0.1, "mmi":-0.2}

    logger.info("Retrieving all annees and ppmm codes")
    skip_annee_naissance_start = year_start-70
    all_annees = []
    all_tags = []
    all_annees_code = []
    #all_colors_ppmm = []
    for a in tsv_vals:
        code = a["code"]
        if not (code.startswith("ppa") or code.startswith("mma") or code.startswith("ppi") or code.startswith("mmi")):
            continue
        else:
            mmpp_code = code
            
        #logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        try:
            val_naissance_str = (a["naissance_year"]).strip("\n").strip(" ")
            val_deces_str = (a["deces_year"]).strip("\n").strip(" ")
            val_nom_str = (a["nom"]).strip("\n").strip(" ")
            val_prenom_str = (a["prenom"]).strip("\n").strip(" ")
            age_start = year_start-int(val_naissance_str)
            if age_start<0:
                age_start = 0
            age_stop = year_stop-int(val_naissance_str)
            tag="{} {} : {}-{} ans".format(val_prenom_str, val_nom_str, age_start, age_stop)
            if len(val_naissance_str)>0 and int(val_naissance_str)>skip_annee_naissance_start: 
                if len(val_deces_str)>0:
                    year_deces = int(val_deces_str)
                else:
                    year_deces = int(2100)

                is_alive = year_deces>=year_start and int(val_naissance_str)<year_stop

                if is_alive:
                    all_annees.append({"naissance":int(val_naissance_str), "deces":year_deces})
                    all_annees_code.append("{}@{}".format(val_naissance_str, mmpp_code))
                    all_tags.append(tag)
                    #all_colors_ppmm.append(colors_ppmm_d[mmpp_code])
        except:
            pass

    logger.info("all_annees found for this period: {}".format(all_annees))
    if len(all_annees)<1:
        return

    all_generations = []
    fig = plt.figure("chronologie_{}_{}".format(year_start, year_stop), figsize=(14, 11))
    ax1 = plt.subplot(1, 1, 1)
    
    sns.set_theme()
    sns.set_style("white")

    ycoords_unique = []
    ycoords_step = 1
    logger.info("all_annees: {}".format(all_annees))

    all_annees_l = [a["naissance"] for a in all_annees]
    argsorted_l = np.argsort(all_annees_l)

    mmpp_annees = []
    mmpp_generation_k = []
    mmpp_code = []
    mmpp_tag = []
    sexe_marker_l = []
    for ka in argsorted_l[::-1]:
        rawcode = all_annees_code[ka]
        code = rawcode.split("@")[1]
        mmpp_code.append(code)
        mmpp_tag.append(all_tags[ka])
        mmpp_annees.append([all_annees[ka]["naissance"],all_annees[ka]["deces"]])
        gene_k = len(code)-4
        if gene_k==-1:
            gene_k = 0
            if code.startswith("m"):
                sexe_marker = "+"
            else:
                sexe_marker = "o"
        else:
            if code.endswith("m"):
                sexe_marker = "+"
            else:
                sexe_marker = "o"
        if len(ycoords_unique)>0:
            last_ycoord = ycoords_unique[-1]
        else:
            last_ycoord = 0
        ycoords_unique.append(last_ycoord+ycoords_step)

        mmpp_generation_k.append(gene_k)
        all_generations.append(gene_k)
        sexe_marker_l.append(sexe_marker)
                
        
    logger.info("mmpp_annees: n={}, {}".format(len(mmpp_annees), mmpp_annees))
    logger.info("mmpp_generation_k: n={}, {}".format(len(mmpp_generation_k),mmpp_generation_k))
    for km in range(len(mmpp_annees)):
        if sexe_marker_l[km]=="+":
            marker_size = 12
            alpha = 0.9
        else:
            marker_size = 8
            alpha = 0.6
        color = get_color_bycode(mmpp_code[km])
        ax1.plot(mmpp_annees[km], 
                [ycoords_unique[km], 
                ycoords_unique[km]], 
                marker=sexe_marker_l[km], 
                markersize=marker_size, 
                color=color, 
                alpha=alpha)
        text_xpos = mmpp_annees[km][0]
        if text_xpos>year_start:
            text_xpos = year_start
        ax1.text(text_xpos, ycoords_unique[km], "({}) {}    ".format(mmpp_generation_k[km], mmpp_tag[km]), ha="right", va="center", color=color)
        

    sns.despine(offset=10, trim=True)

    man_patch = mpatches.Patch(color='black', alpha=0.0, label='o Homme')
    woman_patch = mpatches.Patch(color='black', alpha=0.0, label='+ Femme')
    sep_patch = mpatches.Patch(color='black', alpha=0.0, label=' ')

    blue_patch = mpatches.Patch(color='darkblue', label=labels_ppmm_d['ppa'])
    lightnlue_patch = mpatches.Patch(color='lightblue', label=labels_ppmm_d['mma'])
    red_patch = mpatches.Patch(color='red', label=labels_ppmm_d['ppi'])
    orange_patch = mpatches.Patch(color='orange', label=labels_ppmm_d['mmi'])


    gen_patch = mpatches.Patch(color='black', alpha=0.0, label='(k) Generation, Grand-parent + k')
    plt.legend(frameon=False,
                loc='lower left', 
                handles=[blue_patch, lightnlue_patch, red_patch, orange_patch, sep_patch, man_patch, woman_patch, gen_patch], 
                ncol=1)

    #ax1.set_ylabel("Génération (grand-parent + k)\n")
    ax1.get_yaxis().set_visible(False)

    ax1.set_xlabel("\n")

    ax1.set_title("{}\n\n".format(title_tag))

    ymax_dashed_line = np.max(ycoords_unique)+1
    ax1.plot([year_start, year_start], [0, ymax_dashed_line], linestyle="dashed", color="black", alpha = 0.6)
    ax1.plot([year_stop, year_stop], [0, ymax_dashed_line], linestyle="dashed", color="black", alpha = 0.6)
    ax1.text(year_start, ymax_dashed_line + .1, "{}".format(year_start), ha="right")
    ax1.text(year_stop, ymax_dashed_line + .1, "{}".format(year_stop), ha="left")
    #plt.minorticks_on()
    #plt.grid(b=True, which='major', linewidth=.5)
    #plt.grid(b=True, which='minor', linewidth=0.1)
    #major_ticks = np.arange(np.min(all_generations)-1, np.max(all_generations)+1)
    #ax1.set_yticks(major_ticks)

    ax1.set_xlim([year_start-120, year_stop+20])


    if out_dirpath is not None:
        if not os.path.isdir(out_dirpath):
            os.mkdir(out_dirpath)
        out_fpath = os.path.join(out_dirpath, "chronolo_{}_{}.png".format(year_start, year_stop))
        plt.savefig(out_fpath, dpi=400)
    else:
        plt.show()


def display_age_deces(tsv_vals, out_dirpath=None):
    """
    """
    age_per_generation_persexe = {}
    age_per_generation_persexe["homme"]={}
    age_per_generation_persexe["femme"]={}
    all_age_naissance_enfant = []
    all_generations = []
    for a in tsv_vals:
        code = a["code"]
        if not (code.startswith("ppa") or code.startswith("mma") or code.startswith("ppi") or code.startswith("mmi")):
            continue
            
        logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        age_deces = None
        try:
            val_naissance_int = int(a["naissance_year"])
            val_deces_int = int(a["deces_year"])
            age_deces = val_deces_int-val_naissance_int
        except:
            continue

        if age_deces is not None:
            generation = len(code)-4
            if generation==-1:
                generation = 0
                if code.startswith("m"):
                    sexe = "femme"
                else:
                    sexe = "homme"
            else:
                if code.endswith("m"):
                    sexe = "femme"
                else:
                    sexe = "homme"

            all_generations.append(generation)
            logger.info("for code={}, generation={}".format(code, generation))
            logger.info("age_deces: {}".format(age_deces))


            if not generation in age_per_generation_persexe[sexe].keys():
                age_per_generation_persexe[sexe][generation] = []
            age_per_generation_persexe[sexe][generation].append(age_deces)
            
            logger.info("age_per_generation: {}".format(age_per_generation_persexe[sexe]))

    sns.set_theme()
    sns.set_style("white")
    #sns.set_context("talk")
    fig = plt.figure(figsize=(12, 6))
    ax1 = plt.subplot(1, 1, 1)
    #xval = range(len(list(maxconfidence_d.keys())))
    #xval = [msa_max_nover25_d[key] for key in maxconfidence_d.keys()]
    #yval = [maxconfidence_d[key] for key in maxconfidence_d.keys()]
    
    for sexe in ["homme", "femme"]:
        if sexe=="homme":
            color="blue"
        else:
            color="red"
        avg_ages_yvals = []
        avg_ages_yerrs = []
        avg_gen_xvals = []
        keys_gene_sorted = natsorted(age_per_generation_persexe[sexe].keys())
        for keygen in keys_gene_sorted:
            xvals = np.ones(len(age_per_generation_persexe[sexe][keygen]))*int(keygen)
            yvals = age_per_generation_persexe[sexe][keygen]
            ax1.plot(xvals, yvals, "x", color=color)
            avg_gen_xvals.append(np.mean(xvals))
            avg_ages_yvals.append(np.mean(yvals))
            avg_ages_yerrs.append(np.std(yvals))
        ax1.errorbar(avg_gen_xvals, avg_ages_yvals, yerr=avg_ages_yerrs, color=color, label="Moyenne")
    #ax1.set_title('Age du parent à la naissance de l\'enfant\n')
    ax1.set_xlabel("Génération (grand-parent + k)\n")
    #ax1.set_xscale('log')
    ax1.set_ylabel("Espérance de vie\n(Années)")
    plt.minorticks_on()
    plt.grid(b=True, which='major', linewidth=.5)
    #plt.grid(b=True, which='minor', linewidth=0.1)
    logger.info("all_generations for ticks: {}".format(all_generations))
    if len(all_generations)<1:
        return
    major_ticks = np.arange(np.min(all_generations)-1, np.max(all_generations)+1)
    ax1.set_xticks(major_ticks)

    ax1.set_xlim([np.min(all_generations)-0.5, np.max(all_generations)+.5])

    red_patch = mpatches.Patch(color='red', label='Femme')
    blue_patch = mpatches.Patch(color='blue', label='Homme')
    plt.legend(handles=[red_patch, blue_patch], ncol=2)

    sns.despine(offset=10, trim=True)
    plt.tight_layout()

    if out_dirpath is not None:
        if not os.path.isdir(out_dirpath):
            os.mkdir(out_dirpath)
        out_fpath = os.path.join(out_dirpath, "age_deces.png")
        plt.savefig(out_fpath, dpi=300)
    else:
        plt.show()

        

def get_info_parent(tsv_vals, code, key_info="naissance_year", type="int"):
    """
    """
    if not (code.startswith("ppa") or code.startswith("mma") or code.startswith("ppi") or code.startswith("mmi")):
        logger.info("info parent: code not found")
        return []

    parents_code = []
    parents_info_l = []
    for a in tsv_vals:

        _code = a["code"]
        #logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        try:
            val_str = a[key_info]
            if type=="int":
                val_finale = int(val_str)
            else:
                val_finale = val_str

            #logger.info("OK: input_code: <{}>, candidate_code: <{}>".format(code, _code))
        except:
            #logger.info("EXCEPT: input_code: <{}>, candidate_code: <{}>".format(code, _code))
            continue

        if _code == "{}p".format(code) or _code == "{}_p".format(code) or _code=="{}m".format(code) or _code=="{}_m".format(code):
            #logger.info("found_parent")
            parents_info_l.append({"year":val_finale, "code":_code})
            parents_code.append(_code)

    if len(parents_code)>0:
        logger.info("for code: {}, found parents: {}, year: {}".format(code, parents_code, parents_info_l))

    return parents_info_l


def display_prenom_wordcloud(tsv_vals, out_dirpath=None):
    """
    """
    all_prenoms = []
    for a in tsv_vals:
        #logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        p_split = a["prenom"].split(",")
        for spl in p_split:
            #logger.info("spl={}".format(spl))
            clean_str = spl.strip(" ")
            all_prenoms.append(clean_str)
    logger.info("all prenoms: {}".format(all_prenoms))

    text = " ".join(all_prenoms)
    nprenoms = len(all_prenoms)

    exclure_mots = ['d', 'du', 'de', 'la', 'des', 'le', 'et', 'est', 'elle', 'une', 'en', 'que', 'aux', 'qui', 'ces', 'les', 'dans', 'sur', 'l', 'un', 'pour', 'par', 'il', 'ou', 'à', 'ce', 'a', 'sont', 'cas', 'plus', 'leur', 'se', 's', 'vous', 'au', 'c', 'aussi', 'toutes', 'autre', 'comme']
    wordcloud = WordCloud(background_color='white', stopwords=exclure_mots, max_words=500, width=1200, height=800, 
                            relative_scaling=0.7,repeat=0, min_font_size=4,
                            collocations=False).generate(text)

    plt.imshow(wordcloud)
    plt.title("Prénoms ({})\n\n".format(nprenoms))
    plt.axis("off")
    if out_dirpath is None:
        plt.show();
    else:
        if not os.path.isdir(out_dirpath):
            os.mkdir(out_dirpath)
        out_png_fpath = os.path.join(out_dirpath, "wordcloud_prenoms.png") 
        plt.savefig(out_png_fpath, dpi=300)

def display_nom_wordcloud(tsv_vals, out_dirpath=None):
    """
    """
    all_noms = []
    for a in tsv_vals:
        #logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        p_split = a["nom"].split(",")
        for spl in p_split:
            #logger.info("spl={}".format(spl))
            clean_str = spl.strip(" ")
            all_noms.append(clean_str)
    all_noms = sorted(all_noms)
    set_noms = list(set(all_noms))
    logger.info("all noms: {}".format(all_noms))

    text = " ".join(all_noms)
    nnoms = len(all_noms)

    exclure_mots = ['d', 'du', 'de', 'la', 'des', 'le', 'et', 'est', 'elle', 'une', 'en', 'que', 'aux', 'qui', 'ces', 'les', 'dans', 'sur', 'l', 'un', 'pour', 'par', 'il', 'ou', 'à', 'ce', 'a', 'sont', 'cas', 'plus', 'leur', 'se', 's', 'vous', 'au', 'c', 'aussi', 'toutes', 'autre', 'comme']
    wordcloud = WordCloud(background_color='white', 
                            stopwords=exclure_mots, 
                            max_words=len(set_noms), 
                            width=1200, 
                            height=800, 
                            relative_scaling=0.7,repeat=0, min_font_size=4,
                            collocations=False).generate(text)

    plt.imshow(wordcloud)
    plt.title("Noms ({})\n\n".format(nnoms))
    plt.axis("off")
    if out_dirpath is None:
        plt.show();
    else:
        if not os.path.isdir(out_dirpath):
            os.mkdir(out_dirpath)
        out_png_fpath = os.path.join(out_dirpath, "wordcloud_noms.png") 
        plt.savefig(out_png_fpath, dpi=300)
        
def display_metier_wordcloud(tsv_vals, out_dirpath=None):
    """
    """
    all_noms = []
    for a in tsv_vals:
        #logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        metier_str = a["metier"].strip(" ").strip("\n").replace("  ", "_").replace(" ", "_")
        metier_str.replace("__", "_")
        if metier_str=="-":
            continue
        all_noms.append(metier_str)
    logger.info("all metiers: {}".format(all_noms))
    if len(all_noms)<1:
        return

    text = " ".join(all_noms)
    nnoms = len(all_noms)

    exclure_mots = ['d', 'du', 'de', 'la', 'des', 'le', 'et', 'est', 'elle', 'une', 'en', 'que', 'aux', 'qui', 'ces', 'les', 'dans', 'sur', 'l', 'un', 'pour', 'par', 'il', 'ou', 'à', 'ce', 'a', 'sont', 'cas', 'plus', 'leur', 'se', 's', 'vous', 'au', 'c', 'aussi', 'toutes', 'autre', 'comme']
    wordcloud = WordCloud(background_color='white', stopwords=exclure_mots, max_words=500, width=1200, height=800, 
                            relative_scaling=0.7,repeat=0, min_font_size=4, prefer_horizontal=0.7,
                            collocations=False).generate(text)

    plt.imshow(wordcloud)
    plt.title("Métiers ({})\n\n".format(nnoms))
    plt.axis("off")
    if out_dirpath is None:
        plt.show();
    else:
        if not os.path.isdir(out_dirpath):
            os.mkdir(out_dirpath)
        out_png_fpath = os.path.join(out_dirpath, "wordcloud_metiers.png") 
        plt.savefig(out_png_fpath, dpi=300)

def display_naissance_ville_wordcloud(tsv_vals, out_dirpath=None):
    """
    """
    all_noms = []
    for a in tsv_vals:
        #logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        naissance_str = a["naissance_ville"].strip(" ").strip("\n").replace("-", "_").replace(" ", "_")
        naissance_str.replace("__", "_")
        if len(naissance_str)>0:
            all_noms.append(naissance_str)
    logger.info("all villes: {}".format(all_noms))

    if len(all_noms)<1:
        return

    text = " ".join(all_noms)
    nnoms = len(all_noms)

    exclure_mots = ['d', 'du', 'de', 'la', 'des', 'le', 'et', 'est', 'elle', 'une', 'en', 'que', 'aux', 'qui', 'ces', 'les', 'dans', 'sur', 'l', 'un', 'pour', 'par', 'il', 'ou', 'à', 'ce', 'a', 'sont', 'cas', 'plus', 'leur', 'se', 's', 'vous', 'au', 'c', 'aussi', 'toutes', 'autre', 'comme']
    wordcloud = WordCloud(background_color='white', stopwords=exclure_mots, max_words=500, width=1200, height=800, 
                            relative_scaling=0.7,repeat=0, min_font_size=4,
                            collocations=False).generate(text)

    plt.imshow(wordcloud)
    plt.title("Naissance ({})\n\n".format(nnoms))
    plt.axis("off")
    if out_dirpath is None:
        plt.show();
    else:
        if not os.path.isdir(out_dirpath):
            os.mkdir(out_dirpath)
        out_png_fpath = os.path.join(out_dirpath, "wordcloud_naissance_ville.png") 
        plt.savefig(out_png_fpath, dpi=300)


def get_tsv_col_mapping():
    """
    """
    col_mapping_d = {}
    col_mapping_d["nom"] = 0
    col_mapping_d["prenom"] = 1
    col_mapping_d["code"] = 2
    col_mapping_d["metier"] = 3
    col_mapping_d["naissance_year"] = 5
    col_mapping_d["naissance_ville"] = 6
    col_mapping_d["naissance_pays"] = 7
    col_mapping_d["deces_year"] = 9

    return col_mapping_d
    

def convert_to_percode_dico(tsv_vals):
    """
    use main key: ppmmcode
    store columns: 
        - prenom
        - naissance_annee
    """
    vals_percode_d = {}

    for a in tsv_vals:
        code = a["code"]
        if not (code.startswith("ppa") or code.startswith("mma") or code.startswith("ppi") or code.startswith("mmi")):
            continue
        else:
            mmpp_code = code[:3]
            
        #logger.info("a={}".format(a))
        #logger.info("prenom={}".format(a["prenom"]))
        try:
            vals_percode_d[code] = a.copy()
        except:
            pass

    return vals_percode_d
    

def load_tsv_gene(in_tsv_fpath, exclude_mmppcode=None):
    """
    """
    col_mapping_d = get_tsv_col_mapping()

    all_vals = []
    if in_tsv_fpath is not None:
        with open(in_tsv_fpath, "r") as fin:
            for line in fin:
                line = line.lstrip(" ")
                if not line.startswith("#"):
                    line_split = line.split("\t")
                    
                    skip = False
                    if exclude_mmppcode is not None and len(exclude_mmppcode)>0:
                        for code_prefix in exclude_mmppcode:
                            if line_split[col_mapping_d["code"]].startswith(code_prefix):
                                skip = True
                    if not skip:
                        vals_d = {}
                        for key in col_mapping_d.keys():
                            logger.info("for key={}".format(key))
                            val = (line_split[col_mapping_d[key]]).strip("\n").strip(" ")
                            #logger.info("for key={}, val={}".format(key, val))
                            vals_d[key] = val

                        all_vals.append(vals_d)

        logger.info("Loaded n={}".format(len(all_vals)))
        #logger.info("Loaded l={}".format((all_vals)))
    else:
        all_vals = None

    return all_vals


if __name__ == "__main__":
    """
    """
    logger.info("Starting display now.")

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, description="""
                            Display stats:
                            """)

    parser.add_argument('-i', action="store", dest="in_genetsv_fpath", default="./gene.tsv", help="path to the input tsv file")
    
    parser.add_argument('-c', action="store", dest="in_config_fpath", default=None, help="path to the config input file")
    parser.add_argument('-o', action="store", dest="output_path", default=None, help="path to the output directory")

    args = parser.parse_args()

    if os.path.exists(args.in_genetsv_fpath):
        in_genetsv_fpath = os.path.abspath(args.in_genetsv_fpath)
    else:
        logger.error("input tsv file does not exist. aborting")
        exit()

    process(in_genetsv_fpath=in_genetsv_fpath,
            in_config_fpath=args.in_config_fpath,
            output_path=args.output_path)