
## install
### get the code
<pre>
mkdir libgenestats
cd libgenestats
git clone https://gitlab.com/al1schmitt/geneastats.git
</pre>

### get the pycircos code
<pre>
git clone https://github.com/KimBioInfoStudio/PyCircos.git
cd PyCircos
pip install -r requirements.txt
python3 setup.py install --user
</pre>


### Install the package in dev. mode
Once the code has been cloned into its local directory, it can be installed in a virtual environnement, as follows:
<pre>
cd libgenestats/
virtualenv -p /usr/bin/python3.9 venv_genestats
(alternatively: python3 -m venv venv_genestats)
source venv_genestats/bin/activate
pip3 install -r requirements.txt
pip3 install -e ./
</pre>

## prepare the data table
### convert from gedcom
- import gedcom in GRAMPS software
- export in csv format
- convert from this csv format using the convert script:
>python3 pygenestats/convert_gramps_csv.py -i exported_from_gramps.csv -o output_converted.tsv -t gramps -c convert_grandparents_example.config
use a config file to define the grandparents to be used (see example file: example/convert_grandparents_example.config)

### manually
- see the example tree and table
- "Prenoms" should be separated by a coma
- "Code" is built like this:
   <prefix>_<code>, with the prefix corresponding to one of the 4 grand-parents : ppa, mma, ppi, mmi (for papapa, mamama, papi, mami, in french...)
   and the code corresponding to the navigation in the grand-parent branch, p=papa, m=maman

## run the example
python3 pygenestats/display_stats_all.py -i example/table_example.csv 


